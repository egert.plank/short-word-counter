// Include the File System module
const fs = require('fs');

// Function to count words. Return map. Keys are words, values are counts.
export function count(inputFile: string): Map<string, number> {
    // Store words and their count as a map where key is word (from the file) and value is number (count of the word)
    let map = new Map<string, number>();
    // Read existing given file
    let file = fs.readFileSync(inputFile,'utf8');
    // All words from given file - Trim all unnecessary spaces, split words, remove whitespaces and finally, sort alphabetically
    let words: string[] = file.trim().split(/\s+/).map(function(word: any) {
        // Remove marks and signs from the beginning and from the end of each word and turn the word to lower case
        return word.replace(/\,$|\.$|\:$|\;$|\?$|\!$|\)$|\"$|\'$|^\(|^\"|^\'/g, '').toLowerCase();
     }).sort();
    // Loop through all words. If map includes given word we increase its value by 1, otherwise we set the word as a key and 1 as a value
    for (let word of words){
        map.has(word) ? map.set(word, map.get(word)! + 1) : map.set(word, 1);
        }
    return map;
}

if (require.main === module) {
    // Our file is 3rd argument in the command line. Use it as a parameter to run the program and console log results
    let inputFile = process.argv[2];
    count(inputFile).forEach((value: number, key: string) => {
        if (key != '' ) console.log(`${key}: ${value}`);
        else console.log(`'${inputFile}' does not contain any words!`);
    });
}