## Name
Short Word Counter



## Description
Adaptavist Junior Software Engineer
Task 1 - JavaScript/TypeScript Word Count Challenge

Word Counter is a simple application that reads a text content from a file, counts each unique word, and prints the output into the console.
Some very basic tests are added to the project. 

Technologies used: Node.js, TypeScript, Jest.



## Instructions to Run and Build
Please make sure you are located in the project folder.
To run the app, run 
`ts-node word-counter.ts /path/to/file` or `ts-node no-input-check.ts /path/to/file`
in your terminal.
For example: "ts-node word-counter.js ./test/input5.txt".

To run tests, run: 
`npm test`.
