import {countWords} from './word-counter';

// To ignore console logs while testing
jest.spyOn(console, 'log').mockImplementation(jest.fn());

// Exptected count, real count
let expectedCount: number;
// Actual count from our program
let actualCount: number;

// Testing whitespaces 
test('Whitespaces are removed correctly', () => {
    // Our result: how many different words our map include?
    actualCount = countWords('./test/input1.txt').size;
    // Expected/real amount
    expectedCount = 22;
    // Compare results
    expect(actualCount).toBe(expectedCount);
});

// Testing marks and signs
test("Marks/signs (',' '.' '!' '?' ':' ';') are removed correctly", () => {
    // Our result: how many different words our map include?
    actualCount = countWords('./test/input2.txt').size;
    // Expected/real amount
    expectedCount = 4;
    // Compare results
    expect(actualCount).toBe(expectedCount);
});

//Testing case sensitivity
test('Count the same words regardless of the case sensitivity', () => {
    let map = countWords('./test/input3.txt');
    // We expect that we have 1 word as a key
    actualCount = map.size;
    expectedCount = 1;
    // Compare results
    expect(actualCount).toBe(expectedCount);
    // And we expect to have 12 as a value. Test file contains
    // 12 * 'adaptavist', but every word is written differently
    actualCount = map.get('adaptavist')!;
    expectedCount = 12;
    // Compare results
    expect(actualCount).toBe(expectedCount);
});

// Testing file without words
test('Testing file without words', () => {
    // Count how much empty string we get
    // PS! Empty strings are not logged/displayed when running the program
    let emptyWord = countWords('./test/input6.txt').get('');
    expectedCount = 6;
    // Compare results
    expect(emptyWord).toBe(expectedCount);
});

// Testing text
test('Some more basic tests...', () => {
    actualCount = countWords('./test/input4.txt').size;
    expectedCount = 66;
    // Compare results
    expect(actualCount).toBe(expectedCount);
});

// Another text testing
test('And some more tests...', () => {
    actualCount = countWords('./test/input5.txt').size;
    expectedCount = 85;
    // Compare results
    expect(actualCount).toBe(expectedCount);
});

