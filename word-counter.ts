// Include the File System module
const fs = require('fs');

// Exporting countWords function that returns map of words and counts
export function countWords(inputFile: string): Map<string, number> {
    // Store words and their count as a map where key is word (from the file) and value is number (count of the word)
    let map = new Map<string, number>();
    // If nothing was found as a third argument, let user know
    if (!inputFile) console.log("\nPlease enter filename!");
    // If entered filename was not with '.txt' extension
    else if (inputFile.substring(inputFile.length - 4) != '.txt') wrongExtension(inputFile);
    // If user entered a correct filename but it doesn't exist, we let user know
    else if (!fs.existsSync(inputFile)) console.log("\nFile not found!");
    // Else, file was found, lets count words! 
    else map = letsCount(inputFile, map);
    return map;
}

// Function to re-run program with '.txt' extension
function wrongExtension(inputFile: string) {
    // Let user know
    console.log("\nEntered file was not with '.txt' extension.");
    // Add '.txt' extension
    inputFile += '.txt';
    // Log that program will try to re-run with given input, but with '.txt' extension
    console.log(`\nTrying to open '${inputFile}'...\n`);
    // Re-run function with new filename
    countWords(inputFile).forEach((value: number, key: string) => {
        // Cleaning words with regular expressions can cause empty words
        if (key != '') console.log(`${key}: ${value}`);
        // Else if key = '' and map size is 1, means file does not contain any words and we alert user
        else if (countWords(inputFile).size == 1) console.log(`'${inputFile}' does not contain any words!`);
    });
}

// Function to count words. Return map. Keys are words, values are counts.
function letsCount(inputFile: string, map: Map<string, number>): Map<string, number> {
    // Read existing given file
    let file = fs.readFileSync(inputFile,'utf8');
    // All words from given file - Trim all unnecessary spaces, split words and remove whitespaces
    let words: string[] = file.trim().split(/\s+/).map(function(word: any) {
        // Remove marks and signs from the beginning and from the end of each word and turn the word to lower case
        return word.replace(/\,$|\.$|\:$|\;$|\?$|\!$|\)$|\"$|\'$|^\(|^\"|^\'/g, '').toLowerCase();
     });    
    // Sort words alphabetically
    words.sort((a, b) => a.localeCompare(b));
    // Loop through all words. If map includes given word we increase its value by 1, otherwise we set the word as a key and 1 as a value
    for (let word of words){
        map.has(word) ? map.set(word, map.get(word)! + 1) : map.set(word, 1);
        }
    return map;
}

// Our file is 3rd argument in the command line. Use it as a parameter to run the program and console log results
let inputFile = process.argv[2];
countWords(inputFile).forEach((value: number, key: string) => {
    if (key != '' ) console.log(`${key}: ${value}`);
    else console.log(`'${inputFile}' does not contain any words!`);
});